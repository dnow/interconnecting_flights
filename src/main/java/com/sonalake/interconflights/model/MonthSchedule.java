package com.sonalake.interconflights.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MonthSchedule {

    private final int month;
    private final List<DaySchedule> days;

    @JsonCreator
    public MonthSchedule(@JsonProperty("month") int month, @JsonProperty("days") List<DaySchedule> days) {
        this.month = month;
        this.days = days;
    }

    public int getMonth() {
        return month;
    }

    public List<DaySchedule> getDays() {
        return days;
    }
}
