package com.sonalake.interconflights.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RouteData {

	private final Route route;
	private final boolean newRoute;
	private final boolean seasonalRoute;

	@JsonCreator
	public RouteData(@JsonProperty("airportFrom") String airportFrom, @JsonProperty("airportTo") String airportTo,
			@JsonProperty("newRoute") boolean newRoute, @JsonProperty("seasonalRoute") boolean seasonalRoute) {
		this.route = new Route(airportFrom, airportTo);
		this.newRoute = newRoute;
		this.seasonalRoute = seasonalRoute;
	}

	public Route getRoute() {
		return route;
	}

	public boolean isNewRoute() {
		return newRoute;
	}

	public boolean isSeasonalRoute() {
		return seasonalRoute;
	}
}
