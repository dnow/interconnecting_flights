package com.sonalake.interconflights.model;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sonalake.interconflights.utils.JsonDateTimeDeserializer;
import com.sonalake.interconflights.utils.JsonDateTimeSerializer;

@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY, fieldVisibility = JsonAutoDetect.Visibility.NONE)
@JsonPropertyOrder({ "departureAirport", "arrivalAirport", "departureDateTime", "arrivalDateTime" })
public class FlightSchedule {

	private final String departureAirport;
	private final String arrivalAirport;
	private final LocalDateTime departureDateTime;
	private final LocalDateTime arrivalDateTime;

	@JsonCreator
	public FlightSchedule(@JsonProperty("departureAirport") String departureAirport,
			@JsonProperty("arrivalAirport") String arrivalAirport,
			@JsonProperty("departureDateTime") @JsonDeserialize(using=JsonDateTimeDeserializer.class) LocalDateTime departureDateTime,
			@JsonProperty("arrivalDateTime")  @JsonDeserialize(using=JsonDateTimeDeserializer.class) LocalDateTime arrivalDateTime) {
		this.departureAirport = departureAirport;
		this.arrivalAirport = arrivalAirport;
		this.departureDateTime = departureDateTime;
		this.arrivalDateTime = arrivalDateTime;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public LocalDateTime getDepartureDateTime() {
		return departureDateTime;
	}

	@JsonSerialize(using = JsonDateTimeSerializer.class)
	public LocalDateTime getArrivalDateTime() {
		return arrivalDateTime;
	}

	@Override
	public String toString() {
		return "FlightSchedule [departureAirport=" + departureAirport + ", arrivalAirport=" + arrivalAirport
				+ ", departureDateTime=" + departureDateTime + ", arrivalDateTime=" + arrivalDateTime + "]";
	}
	
}
