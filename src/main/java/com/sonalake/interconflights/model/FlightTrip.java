package com.sonalake.interconflights.model;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FlightTrip {
    private final int stops;
    private final List<FlightSchedule> legs;

    @JsonCreator
    public FlightTrip(@JsonProperty("stops") int stops, @JsonProperty("legs") List<FlightSchedule> legs) {
        this.stops = stops;
        this.legs = legs;
    }

    public int getStops() {
        return stops;
    }

    public List<FlightSchedule> getLegs() {
        return legs;
    }
    
    public FlightSchedule firstFlight() {
        return legs.get(0);
    }
    
    public FlightSchedule lastFlight() {
        return legs.get(legs.size() - 1);
    }
    
    public Duration getTripDuration() {
    	LocalDateTime start = firstFlight().getDepartureDateTime();
    	LocalDateTime end = lastFlight().getArrivalDateTime();
    	return Duration.between(start, end);
    }

	@Override
	public String toString() {
		return "FlightTrip [stops=" + stops + ", legs=" + legs + "]";
	}
    
}
