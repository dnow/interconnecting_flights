package com.sonalake.interconflights.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DaySchedule {

    private final int day;
    private final List<Flight> flights;

    @JsonCreator
    public DaySchedule(@JsonProperty("day") int day, @JsonProperty("flights") List<Flight> flights) {
        this.day = day;
        this.flights = flights;
    }

    public int getDay() {
        return day;
    }

    public List<Flight> getFlights() {
        return flights;
    }
}
