package com.sonalake.interconflights.service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.jgrapht.DirectedGraph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.AllDirectedPaths;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sonalake.interconflights.client.FlightApiClient;
import com.sonalake.interconflights.model.Route;

@Service
public class RoutePathFindServiceImpl implements RoutePathFindService {

	private FlightApiClient flightApiClient;

	@Autowired
	public RoutePathFindServiceImpl(FlightApiClient flightApiClient) {
		this.flightApiClient = flightApiClient;
	}
	
	public static DirectedGraph<String, DefaultEdge> convertToGraph(Stream<Route> routes) {
		DirectedGraph<String, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);
		routes.forEach(route -> addRouteToGraph(graph, route));
		return graph;
	}
	
	public static List<Route> pathToRoutes(List<String> path){
		List<Route> joinRoute = new LinkedList<>();
		for (int i = 0; i < path.size() - 1; i++) {
			Route route = new Route(path.get(i), path.get(i + 1));
			joinRoute.add(route);
		}
		return joinRoute;

	}
	
	public static void addRouteToGraph(DirectedGraph<String, ?> graph, Route route) {
		graph.addVertex(route.getDepartureAirport());
		graph.addVertex(route.getArrivalAirport());
		graph.addEdge(route.getDepartureAirport(), route.getArrivalAirport());
	}
	
	@Override
	public Stream<List<Route>> findRoutePaths(final String departureAirport, final String arrivalAirport, final int stops) {
		DirectedGraph<String, DefaultEdge> connections = getRoutesGraph();
		return findRoutePaths(departureAirport, arrivalAirport, connections, stops);
	}

	public DirectedGraph<String, DefaultEdge> getRoutesGraph() {
		return convertToGraph(flightApiClient.getRoutes().stream().map(r -> r.getRoute()));
	}

	protected Stream<List<Route>> findRoutePaths(final String start, final String end,
			final DirectedGraph<String, DefaultEdge> connections, final int stops) {
		
		AllDirectedPaths<String, DefaultEdge> pathFinder = new AllDirectedPaths<>(connections);
		List<GraphPath<String, DefaultEdge>> interconnectedPaths = pathFinder.getAllPaths(start,
				end, true, stops + 1);

		return interconnectedPaths.stream().map(path -> pathToRoutes(path.getVertexList()));
	}
	
}
