package com.sonalake.interconflights.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sonalake.interconflights.client.FlightApiClient;
import com.sonalake.interconflights.model.Flight;
import com.sonalake.interconflights.model.FlightSchedule;
import com.sonalake.interconflights.model.MonthSchedule;
import com.sonalake.interconflights.utils.YearMonthIterable;

@Service
public class FlightScheduleServiceImpl implements FlightScheduleService {

	private FlightApiClient flightApiClient;

	private static final Comparator<FlightSchedule> DEPARTURE_DATE_TIME_COMPARATOR = (flight1, flight2) -> flight1
			.getDepartureDateTime().compareTo(flight2.getDepartureDateTime());

	@Autowired
	public FlightScheduleServiceImpl(FlightApiClient flightApiClient) {
		this.flightApiClient = flightApiClient;
	}

	@Override
	public Stream<FlightSchedule> findFlights(String departureAirport, String arrivalAirport,
			LocalDateTime departureDateTime, LocalDateTime arrivalDateTime) {

		return YearMonthIterable.around(departureDateTime, arrivalDateTime).stream()
				.flatMap(yearMonth -> getFlightSchedules(departureAirport, arrivalAirport, yearMonth))
				.filter(flightSchedule -> isFlightWithinRange(flightSchedule, departureDateTime, arrivalDateTime))
				.sorted(DEPARTURE_DATE_TIME_COMPARATOR);

	}

	private Stream<FlightSchedule> getFlightSchedules(String departureAirport, String arrivalAirport,
			YearMonth yearMonth) {
		Optional<MonthSchedule> schedule = flightApiClient.getSchedule(departureAirport, arrivalAirport, yearMonth);
		return mapToFlightSchedules(departureAirport, arrivalAirport, yearMonth, schedule);
	}

	protected Stream<FlightSchedule> mapToFlightSchedules(String departureAirport, String arrivalAirport,
			YearMonth yearMonth, Optional<MonthSchedule> schedule) {
		if (schedule.isPresent()) {
			return schedule.get().getDays().stream().flatMap(day -> {
				LocalDate date = LocalDate.of(yearMonth.getYear(), yearMonth.getMonth(), day.getDay());
				return day.getFlights().stream()
						.map(flight -> mapToFlightSchedule(departureAirport, arrivalAirport, date, flight));
			});
		}
		return Stream.empty();
	}

	private FlightSchedule mapToFlightSchedule(String departureAirport, String arrivalAirport, LocalDate date,
			Flight flight) {
		LocalDateTime departureDateTime = flight.getDepartureTime().atDate(date);
		LocalDateTime arrivalDateTime = flight.getArrivalTime().atDate(date);
		return new FlightSchedule(departureAirport, arrivalAirport, departureDateTime, arrivalDateTime);
	}

	private boolean isFlightWithinRange(FlightSchedule flightSchedule, LocalDateTime departureDateTime,
			LocalDateTime arrivalDateTime) {
		return !flightSchedule.getDepartureDateTime().isBefore(departureDateTime)
				&& !flightSchedule.getArrivalDateTime().isAfter(arrivalDateTime);
	}
}
