package com.sonalake.interconflights.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sonalake.interconflights.model.FlightSchedule;
import com.sonalake.interconflights.model.FlightTrip;
import com.sonalake.interconflights.model.Route;

@Service
public class FlightSearchServiceImpl implements FlightSearchService {

	private static final int DEFAULT_MAX_STOPS = 1;

	private static final int STOP_MINMUM_TIME_MINUTES = 120;

	private static final Comparator<FlightTrip> TRIP_DURATION_COMPARATOR = Comparator
			.comparing(FlightTrip::getTripDuration);

	@Value("${ryanair.interconnect.time.difference}")
	private int minimumDifference;
	private RoutePathFindService routeService;
	private FlightScheduleService scheduleService;

	@Autowired
	public FlightSearchServiceImpl(RoutePathFindService routeService, FlightScheduleService scheduleService) {
		this.routeService = routeService;
		this.scheduleService = scheduleService;
	}

	@Override
	public List<FlightTrip> searchFlights(String departureAirport, String arrivalAirport,
			LocalDateTime departureDateTime, LocalDateTime arrivalDateTime) {
		final Stream<List<Route>> connections = routeService.findRoutePaths(departureAirport, arrivalAirport,
				DEFAULT_MAX_STOPS);
		// we use parallel stream here to speed up
		return connections.parallel().flatMap(connection -> {
			if (connection.size() == 1) {
				return searchDirectFlight(connection.get(0), departureDateTime, arrivalDateTime);
			} else {
				return searchInterconnectedFlights(connection, departureDateTime, arrivalDateTime);
			}
		}).sorted(TRIP_DURATION_COMPARATOR).collect(Collectors.toList());
	}

	protected Stream<FlightTrip> searchDirectFlight(Route route, LocalDateTime departureDateTime,
			LocalDateTime arrivalDateTime) {

		return scheduleService
				.findFlights(route.getDepartureAirport(), route.getArrivalAirport(), departureDateTime, arrivalDateTime)
				.map(flightSchedule -> new FlightTrip(0, Collections.singletonList(flightSchedule)));

	}

	protected Stream<FlightTrip> searchInterconnectedFlights(List<Route> connection, LocalDateTime departureDateTime,
			LocalDateTime arrivalDateTime) {

		List<List<FlightSchedule>> schedules = connection.stream()
				.map(route -> findRoute(route, departureDateTime, arrivalDateTime)).collect(Collectors.toList());
		return toFlightTrips(schedules, departureDateTime, arrivalDateTime, STOP_MINMUM_TIME_MINUTES);
	}

	protected List<FlightSchedule> findRoute(Route route, LocalDateTime departureDateTime,
			LocalDateTime arrivalDateTime) {
		return scheduleService
				.findFlights(route.getDepartureAirport(), route.getArrivalAirport(), departureDateTime, arrivalDateTime)
				.collect(Collectors.toList());
	}

	protected Stream<FlightTrip> toFlightTrips(List<List<FlightSchedule>> schedules, LocalDateTime departureDateTime,
			LocalDateTime arrivalDateTime, int timeForChangeMinutes) {

		Stream<FlightTrip> trips = schedules.get(0).stream()
				.map(fl -> new FlightTrip(schedules.size() - 1, Lists.newArrayList(fl)));

		for (int i = 1; i < schedules.size(); i++) {
			List<FlightSchedule> singleLegFlights = schedules.get(i);
			trips = trips.flatMap(trip -> continueTrip(trip, singleLegFlights, timeForChangeMinutes, arrivalDateTime));
		}

		return trips;
	}

	protected Stream<FlightTrip> continueTrip(FlightTrip trip, Collection<FlightSchedule> singleLegFlights,
			int timeForChangeMinutes, LocalDateTime arrivalDateTime) {

		LocalDateTime departureDateTime = trip.lastFlight().getArrivalDateTime().plusMinutes(timeForChangeMinutes);
		return singleLegFlights.stream().filter(fs -> isInTime(fs, departureDateTime, arrivalDateTime))
				.map(fs -> toFligtTrip(trip, fs));
	}

	protected boolean isInTime(FlightSchedule fs, LocalDateTime departureDateTime, LocalDateTime arrivalDateTime) {
		return fs.getDepartureDateTime().isAfter(departureDateTime)
				&& fs.getArrivalDateTime().isBefore(arrivalDateTime);
	}

	protected FlightTrip toFligtTrip(FlightTrip trip, FlightSchedule fs) {
		FlightTrip clonedTrip = new FlightTrip(trip.getStops(), new ArrayList<>(trip.getLegs()));
		clonedTrip.getLegs().add(fs);
		return clonedTrip;
	}

}
