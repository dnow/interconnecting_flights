package com.sonalake.interconflights.service;


import java.time.LocalDateTime;
import java.util.stream.Stream;

import com.sonalake.interconflights.model.FlightSchedule;

@FunctionalInterface
public interface FlightScheduleService {

    /**
     * Finds the list of available flights between two dates
     *
     * @param departureAirport
     * @param arrivalAirport
     * @param departureDate
     * @param arrivalDate
     * @return Stream of {@link FlightSchedule}
     */
    Stream<FlightSchedule> findFlights(String departureAirport, String arrivalAirport, LocalDateTime departureDate, LocalDateTime arrivalDate);
}
