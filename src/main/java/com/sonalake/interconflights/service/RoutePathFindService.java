package com.sonalake.interconflights.service;

import java.util.List;
import java.util.stream.Stream;

import com.sonalake.interconflights.model.Route;

@FunctionalInterface
public interface RoutePathFindService {

    /**
     * Find connection between two airports
     *
     * @param departureAirport
     * @param arrivalAirport
     * @return stream of lists {@link Route}
     */
    Stream<List<Route>> findRoutePaths(String departureAirport, String arrivalAirport, int stops);
    
}
