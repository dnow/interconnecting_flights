package com.sonalake.interconflights.service;

import java.time.LocalDateTime;
import java.util.List;

import com.sonalake.interconflights.model.FlightTrip;

@FunctionalInterface
public interface FlightSearchService {

	List<FlightTrip> searchFlights(String departureAirport, String arrivalAirport,
			LocalDateTime departureDateTime, LocalDateTime arrivalDateTime);

}
