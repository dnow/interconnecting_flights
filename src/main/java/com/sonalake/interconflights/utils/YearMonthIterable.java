package com.sonalake.interconflights.utils;

import java.time.YearMonth;
import java.time.temporal.TemporalAccessor;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class YearMonthIterable implements Iterable<YearMonth> {
	
	private YearMonth startYearMonth;
	
	private YearMonth endYearMonth;
	
	public YearMonthIterable(YearMonth startYearMonth, YearMonth endYearMonth) {
		super();
		if(!startYearMonth.isBefore(endYearMonth)) {
			throw new IllegalArgumentException("Start year month must be before end year month");
		}
		this.startYearMonth = startYearMonth;
		this.endYearMonth = endYearMonth;
	}
	
	public static YearMonthIterable around(TemporalAccessor start, TemporalAccessor end) {
		YearMonth startYearMonth = YearMonth.from(start);
		YearMonth endYearMonth = YearMonth.from(end).plusMonths(1);
		return new YearMonthIterable(startYearMonth, endYearMonth);
	}
	
	@Override
	public Iterator<YearMonth> iterator() {
		return new YearMonthIterator(startYearMonth, endYearMonth);
	}
	
	public Stream<YearMonth> stream() {
		return StreamSupport.stream(this.spliterator(), false);
	}
	
	public class YearMonthIterator implements Iterator<YearMonth> {
		
		private YearMonth curentYearMonth;
		
		private YearMonth endYearMonth;
		
		public YearMonthIterator(YearMonth curentYearMonth, YearMonth endYearMonth) {
			super();
			this.curentYearMonth = curentYearMonth;
			this.endYearMonth = endYearMonth;
		}

		@Override
		public boolean hasNext() {
			return curentYearMonth.isBefore(endYearMonth);
		}

		@Override
		public YearMonth next() {
			YearMonth result = curentYearMonth;
			if(result.isAfter(endYearMonth)){
				throw new NoSuchElementException();
			}
			curentYearMonth = curentYearMonth.plusMonths(1);
			return result;
		}
	}

}
