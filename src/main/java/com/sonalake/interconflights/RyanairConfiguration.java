package com.sonalake.interconflights;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.sonalake.interconflights.client.FlightApiClient;
import com.sonalake.interconflights.client.RyanairApiClientImpl;


@Configuration
public class RyanairConfiguration {

    @Bean
    public FlightApiClient ryanairApiClient() {
        return new RyanairApiClientImpl(new RestTemplate());
    }
}
