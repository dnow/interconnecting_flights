package com.sonalake.interconflights.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sonalake.interconflights.model.FlightTrip;
import com.sonalake.interconflights.service.FlightSearchService;

@RestController
public class FlightSearchController {

	@Autowired
	private FlightSearchService flightSearchService;

	@RequestMapping(value = "/interconnections", produces = "application/json")
	public List<FlightTrip> searchFlights(@RequestParam String departure, @RequestParam String arrival,
			@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime departureDateTime,
			@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime arrivalDateTime) {
		
		return flightSearchService.searchFlights(departure, arrival, departureDateTime, arrivalDateTime);
	}
}
