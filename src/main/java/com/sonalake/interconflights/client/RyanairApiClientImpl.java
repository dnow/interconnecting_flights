package com.sonalake.interconflights.client;

import java.time.YearMonth;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.sonalake.interconflights.model.MonthSchedule;
import com.sonalake.interconflights.model.RouteData;

public class RyanairApiClientImpl implements FlightApiClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(RyanairApiClientImpl.class);

	@Value("${ryanair.api.routes}")
	private String routesUrl;

	@Value("${ryanair.api.schedules}")
	private String schedulesUrl;

	private RestTemplate restTemplate;

	public RyanairApiClientImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	@Cacheable("routes")
	public List<RouteData> getRoutes() {
		ResponseEntity<RouteData[]> responseEntity = restTemplate.getForEntity(routesUrl, RouteData[].class);
		return Arrays.asList(responseEntity.getBody());
	}

	@Override
	@Cacheable("schedules")
	public Optional<MonthSchedule> getSchedule(String departureAirport, String arrivalAirport, YearMonth yearMonth) {
		String schedulesApiUrl = String.format(schedulesUrl, departureAirport, arrivalAirport, yearMonth.getYear(),
				yearMonth.getMonthValue());
		MonthSchedule result = null;
		try {
			result = restTemplate.getForObject(schedulesApiUrl, MonthSchedule.class);
		} catch (HttpClientErrorException ex) {
        	if(HttpStatus.NOT_FOUND.equals(ex.getStatusCode())) {
        		LOGGER.debug("Not found for: " + departureAirport + "->" + arrivalAirport + " and " + yearMonth.toString());
        	}else {
        		LOGGER.warn("Ryanair server responded with exception", ex);
        	}
		}
		return Optional.ofNullable(result);
	}
}
