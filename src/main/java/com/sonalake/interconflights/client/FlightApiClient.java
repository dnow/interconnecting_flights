package com.sonalake.interconflights.client;

import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

import com.sonalake.interconflights.model.MonthSchedule;
import com.sonalake.interconflights.model.RouteData;

public interface FlightApiClient {

    /**
     * Returns a list of available routes
     *
     * @return {@link RouteData}
     */
    List<RouteData> getRoutes();

    /**
     * Returns a list of available flights between given airports in specified year and month
     *      
     * @param departureAirport
     * @param arrivalAirport
     * @param yearMonth
     * @return optional value of {@link MonthSchedule}
     */
    Optional<MonthSchedule> getSchedule(String departureAirport, String arrivalAirport, YearMonth yearMonth);
}
