package com.sonalake.interconflights.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.Iterator;

import org.junit.Test;

public class YearMonthIterableTest {

	@Test
	public void testSingleMonth() {
		YearMonthIterable yearMonthIterable = new YearMonthIterable(YearMonth.of(2016, 1), YearMonth.of(2016, 2));
		
		Iterator<YearMonth> iterator = yearMonthIterable.iterator();
		
		assertTrue(iterator.hasNext());
		assertEquals(YearMonth.of(2016, 1), iterator.next());
		assertFalse(iterator.hasNext());
	}
	
	@Test
	public void testMoveToNextYear() {
		YearMonthIterable yearMonthIterable = new YearMonthIterable(YearMonth.of(2015, 12), YearMonth.of(2016, 2));
		
		Iterator<YearMonth> iterator = yearMonthIterable.iterator();
		
		assertTrue(iterator.hasNext());
		assertEquals(YearMonth.of(2015, 12), iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals(YearMonth.of(2016, 1), iterator.next());
		assertFalse(iterator.hasNext());
	}
	
	@Test
	public void testAround() {
		YearMonthIterable yearMonthIterable =  YearMonthIterable.around(YearMonth.of(2016, 1), YearMonth.of(2016, 2));
		
		Iterator<YearMonth> iterator = yearMonthIterable.iterator();
		
		assertTrue(iterator.hasNext());
		assertEquals(YearMonth.of(2016, 1), iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals(YearMonth.of(2016, 2), iterator.next());
		assertFalse(iterator.hasNext());
	}
	
	@Test
	public void testAroundDate() {
		YearMonthIterable yearMonthIterable =  YearMonthIterable.around(LocalDate.of(2016, 1, 30), LocalDate.of(2016, 2, 1));
		
		Iterator<YearMonth> iterator = yearMonthIterable.iterator();
		
		assertTrue(iterator.hasNext());
		assertEquals(YearMonth.of(2016, 1), iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals(YearMonth.of(2016, 2), iterator.next());
		assertFalse(iterator.hasNext());
	}

}
