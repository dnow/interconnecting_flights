package com.sonalake.interconflights.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.sonalake.interconflights.client.FlightApiClient;
import com.sonalake.interconflights.model.Route;
import com.sonalake.interconflights.model.RouteData;

@RunWith(MockitoJUnitRunner.class)
public class RoutePathFindServiceImplTest {
	
	private RoutePathFindServiceImpl service;
	
	private FlightApiClient flightApiClient;

	@Before
	public void setUp() throws Exception {
		flightApiClient = mock(FlightApiClient.class);
		service = new RoutePathFindServiceImpl(flightApiClient);
	}

	@Test
	public void testPathToRoutes() {
		//given
		List<String> paths = Arrays.asList("a", "b", "c");
		//when
		List<Route> routes = RoutePathFindServiceImpl.pathToRoutes(paths);
		//then
		assertEquals("a", routes.get(0).getDepartureAirport());
		assertEquals("b", routes.get(0).getArrivalAirport());
		assertEquals("b", routes.get(1).getDepartureAirport());
		assertEquals("c", routes.get(1).getArrivalAirport());
	}

	@Test
	public void testFindPaths() {
		//given
		when(flightApiClient.getRoutes()).thenReturn(getTestRouteData());
		//when
		List<List<Route>> paths = service.findRoutePaths("a", "c", 1).collect(Collectors.toList());;
		//then
		assertEquals(2, paths.size());
		assertEquals(1, paths.get(0).size());
		validateRoute("a", "c", paths.get(0).get(0));
		assertEquals(2, paths.get(1).size());
		validateRoute("a", "b", paths.get(1).get(0));
		validateRoute("b", "c", paths.get(1).get(1));

	}
	
	@Test
	public void testFindPathsOnlyDirect() {
		//given
		when(flightApiClient.getRoutes()).thenReturn(getTestRouteData());
		//when
		List<List<Route>> paths = service.findRoutePaths("b", "c", 1).collect(Collectors.toList());
		//then
		assertEquals(1, paths.size());
		assertEquals(1, paths.get(0).size());
		validateRoute("b", "c", paths.get(0).get(0));
	}
	
	@Test
	public void testFindPathsToFar() {
		//given
		when(flightApiClient.getRoutes()).thenReturn(getTestRouteData());
		//when
		List<List<Route>> paths = service.findRoutePaths("a", "g", 1).collect(Collectors.toList());
		//then
		assertEquals(0, paths.size());
	}
	
	private void validateRoute(String departure, String arrival, Route r) {
		assertEquals(departure, r.getDepartureAirport());
		assertEquals(arrival, r.getArrivalAirport());
	}
	
	private List<RouteData> getTestRouteData() {
		List<RouteData> result = new ArrayList<>();
		result.add(new RouteData("a", "b", false, false));
		result.add(new RouteData("b", "c", false, false));
		result.add(new RouteData("a", "d", false, false));
		result.add(new RouteData("d", "e", false, false));
		result.add(new RouteData("e", "c", false, false));
		result.add(new RouteData("a", "c", false, false));
		result.add(new RouteData("e", "g", false, false));
		return result;
	}

}
