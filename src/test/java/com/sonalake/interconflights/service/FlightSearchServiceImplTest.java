package com.sonalake.interconflights.service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.sonalake.interconflights.model.FlightSchedule;
import com.sonalake.interconflights.model.FlightTrip;

public class FlightSearchServiceImplTest {
	
	private FlightSearchServiceImpl service = new FlightSearchServiceImpl(null, null);

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testToFlightTrips1stop() {
		List<List<FlightSchedule>> schedules = new ArrayList<>();;
		LocalDateTime departureDateTime = time(0, 1);
		LocalDateTime arrivalDateTime = time(23, 59);
		schedules.add(createFlights("POZ", "WRO", 2, time(1, 11) , time(5, 11), time(20, 11)));
		schedules.add(createFlights("WRO", "DUB", 2, time(4, 12) , time(6, 11), time(19, 11)));
		
		Collection<FlightTrip> flightTrips = service.toFlightTrips(schedules, departureDateTime, arrivalDateTime, 120).collect(Collectors.toList());
		assertEquals(3, flightTrips.size());
	}
	
	@Test
	public void testToFlightTrips1stop2() {
		List<List<FlightSchedule>> schedules = new ArrayList<>();;
		LocalDateTime departureDateTime = time(0, 1);
		LocalDateTime arrivalDateTime = time(23, 59);
		schedules.add(createFlights("POZ", "WRO", 2, time(1, 11) , time(5, 11), time(20, 11)));
		schedules.add(createFlights("WRO", "DUB", 2, time(4, 12) , time(6, 11), time(19, 11)));
		
		Collection<FlightTrip> flightTrips = service.toFlightTrips(schedules, departureDateTime, arrivalDateTime, 60).collect(Collectors.toList());
		assertEquals(4, flightTrips.size());
	}
	
	@Test
	public void testToFlightTrips2stop() {
		List<List<FlightSchedule>> schedules = new ArrayList<>();;
		LocalDateTime departureDateTime = time(0, 1);
		LocalDateTime arrivalDateTime = time(23, 59);
		schedules.add(createFlights("POZ", "WRO", 2, time(1, 11) , time(5, 11), time(20, 11)));
		schedules.add(createFlights("WRO", "DUB", 2, time(4, 12) , time(6, 11), time(19, 11)));
		schedules.add(createFlights("DUB", "LND", 2, time(20, 12) , time(21, 11), time(22, 11)));
		
		Collection<FlightTrip> flightTrips = service.toFlightTrips(schedules, departureDateTime, arrivalDateTime ,120).collect(Collectors.toList());
		assertEquals(2, flightTrips.size());
	}
	
	private LocalDateTime time(int hour, int min) {
		return LocalDateTime.of(2016, 12, 01, hour, min);
	}

	private List<FlightSchedule> createFlights(String from, String to, int flightTime, LocalDateTime ... departureTimes) {
		List<FlightSchedule> result = new ArrayList<>();
		for (LocalDateTime departure : departureTimes) {
			result.add(new FlightSchedule(from, to, departure, departure.plusHours(flightTime)));
		}
		return result;
	}


}
