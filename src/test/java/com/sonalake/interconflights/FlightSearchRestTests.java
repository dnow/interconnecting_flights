package com.sonalake.interconflights;

import static org.assertj.core.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.assertj.core.api.Condition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.sonalake.interconflights.model.FlightTrip;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class FlightSearchRestTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void interconnectionsTest() {
		final LocalDateTime startDate = LocalDateTime.now();
		final LocalDateTime endDate = LocalDateTime.now().plusWeeks(1);
		Map<String, Object> params = new HashMap<>();
		params.put("departure", "WRO");
		params.put("arrival", "DUB");
		params.put("departureDateTime", startDate);
		params.put("arrivalDateTime", endDate);
		FlightTrip[] trips = this.restTemplate.getForObject("/interconnections?departure={departure}&arrival={arrival}&departureDateTime={departureDateTime}&arrivalDateTime={arrivalDateTime}", FlightTrip[].class, params);
		assertThat(trips.length).isGreaterThan(1);
		assertThat(trips).extracting("stops").containsOnly(0, 1);
		assertThat(trips).flatExtracting("legs").extracting("departureAirport").contains("WRO").doesNotContain("DUB");
		assertThat(trips).flatExtracting("legs").extracting("arrivalAirport").contains("DUB").doesNotContain("WRO");
		Condition<LocalDateTime> afterStart = new Condition<LocalDateTime>() {

			@Override
			public boolean matches(LocalDateTime value) {
				return startDate.isBefore(value);
			}
		};
		assertThat(trips).flatExtracting("legs").extracting("departureDateTime").are((Condition)afterStart);
		Condition<LocalDateTime> beforeEnd = new Condition<LocalDateTime>() {

			@Override
			public boolean matches(LocalDateTime value) {
				return endDate.isAfter(value);
			}
		};
		assertThat(trips).flatExtracting("legs").extracting("arrivalDateTime").are((Condition)beforeEnd);
	}

}




