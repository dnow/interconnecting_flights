#Interconnecting Flights
Spring MVC based RESTful API application which serves information about possible direct and interconnected flights (maximum 1 stop) based on the data consumed from external APIs.

##Ryanir api used:
https://api.ryanair.com/core/3/routes/
and 
https://api.ryanair.com/timetable/3/schedules/{departure}/{arrival}/years/{year}/months/{month}


##Prerequisites
- Java 8

## Used technologies
- Java 8
- Spring Boot
- jgrapht
- jackson
- assertj
- mockito

##Build process
 mvn clean package
 
 war can be found inside target dir
 you can use interconnecting_flights-0.0.1-SNAPSHOT.war.original if you care about file size (just rename it to .war)
 
##Usage

`http://<HOST>/<CONTEXT>/interconnections?departure={departure}&arrival={arrival}&departureDateTime={departureDateTime}&arrivalDateTime={arrivalDateTime}`
 where:
- departure - a departure airport IATA code
- departureDateTime - a departure datetime in the departure airport timezone in ISO format
- arrival - an arrival airport IATA code
- arrivalDateTime - an arrival datetime in the arrival airport timezone in ISO format

example:
HTTP GET `localhost:8080/flightsearch/interconnections?departure=WRO&arrival=DUB&departureDateTime=2016-12-21T07:00&arrivalDateTime=2016-12-30T21:00`

##Assumptions

- Implementation search only for 1 stops flight but can be easily changed as we use jgrapht.
- This is just for searching one way flights
- There is no requirement to do this in TDD but added few junit and one integration test. If would have more time would provide 90% test coverage.
- Program uses streams where possible to keep low memory footprint and high peroformance. I also considered using spring reactive but it is still in beta. See more about reactive programing and the future of spring for example here: https://spring.io/blog/2016/07/28/reactive-programming-with-spring-5-0-m1